var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
const cors = require('cors')
const mongoose = require('mongoose')
const expressJWT = require('express-jwt');
var app = express();

const dotenv = require('dotenv');
dotenv.config();
require('dotenv').config();

app.use(cors());
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// app.use(logger('dev'));
app.use(express.json({limit: '25mb'}));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
// app.use(express.static(path.join(__dirname, 'public')));


// template and static files
app.use('/public',express.static('public'))
app.use('/',express.static('planning'))

// models
const users = require('./routes/users')
const prestations = require('./routes/prestations')
const reservations = require('./routes/reservations')
const employes = require('./routes/employes')
const secret = process.env.SECRET_TOKEN;

mongoose
.connect(`mongodb://${process.env.DATABASE_HOST}/mongo-planning`)
.then(() => console.log('connected to MongoDb'))
.catch((err => console.error('could not connect to mongoDb', err)))


app.use(expressJWT({ secret: secret, algorithms: ['HS256'] })
    .unless(
        { path: [
            /^\/api\/users\/signIn/,
            /^\/api\/users\/signUp/,
            /^\/api\/users\/checkToken\/.*/,
            /^\/api\/employes\/.*/,
            /^\/public\/.*/
        ]}
    )  
);


//route
app.use('/api/users', users);
app.use('/api/prestations',prestations)
app.use('/api/reservations',reservations)
app.use('/api/employes',employes)

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
