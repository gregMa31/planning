const mongoose = require('mongoose')

ReservationSchema = new mongoose.Schema({
    date: Date,
    day : String,
    slot: Number,
    id_prestation: String,
    id_employe: String,
    id_user: String
})

const Reservation = mongoose.model('Reservation',ReservationSchema);

async function getAllReservations (){
    return  Reservation.find();
}

async function saveReservation(reservation){
    console.log(reservation);
    const { date, day,slot, id_prestation, id_employe, id_user} = reservation
    const newReservation = new Reservation({
        date,
        day,
        slot,
        id_prestation,
        id_employe,
        id_user,
    })
    return newReservation.save()
}

async function deleteReservationById(_id) {

    Reservation.findByIdAndRemove(_id, function (err) {
        if(err) console.log(err);
        console.log("Successful deletion");
      });
}

exports.getAllReservations = getAllReservations
exports.saveReservation = saveReservation
exports.deleteReservationById = deleteReservationById