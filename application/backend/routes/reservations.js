const {
  getAllReservations,
  saveReservation,
  deleteReservationById,
} = require("../models/reservation");
const express = require("express");
const router = express.Router();

router.get("/reservations/:day/:month/:years", async (req, res) => {
  const date = `${req.params.day}/${req.params.month}/${req.params.years}`;
  let reservations = await getAllReservations();
  let reservationOfCurrentDay = reservations.filter(
    (reservation) => reservation.day === date
  );
  res.status(200).json(reservationOfCurrentDay);
});

router.get("/reservations/userId/:userId", async (req, res) => {
  const userId = `${req.params.userId}`;
  let reservations = await getAllReservations();
  let reservationOfCurrentDay = reservations.filter(
    (reservation) => reservation.id_user === userId
  );
  res.status(200).json(reservationOfCurrentDay);
});

router.post("/reservations/employeId", async (req, res) => {
  const employeId = req.body.employeId;
  const dateRange = req.body.rangeDate;
  const startDate = new Date(dateRange[0]).setHours(0, 0, 0, 0);
  const endDate = new Date(dateRange[1]).setHours(0, 0, 0, 0);

  let reservations = await getAllReservations();
  let reservationOfSelectedEmploye = reservations.filter(
    (reservation) =>
      reservation.id_employe === employeId &&
      new Date(reservation.date).getTime() >= startDate &&
      new Date(reservation.date).getTime() <= endDate
  );

  res.status(200).json(reservationOfSelectedEmploye);
});

router.post("/reservations", async (req, res) => {
  console.log(req.body);
  const newReservation = await saveReservation(req.body);
  const io = req.app.get('socketio');
  io.emit('reservation',newReservation);
  
  res.status(200).json(newReservation);
});

router.get("/reservations/removeReservation/:id", async (req, res) => {
  const id = req.params.id;
  await deleteReservationById(id);
  res.status(200).send({ statusCode: 200, id });
});

module.exports = router;
