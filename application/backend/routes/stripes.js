const express = require("express");
const router = express.Router();
const stripe = require('stripe')('sk_test_51M6pgxJvi3VtEHgUmRc0IE7hjuxGod9nsc0sD8FsvhXNbW8nkoDCUu23L0ixyfIwoSLjk9tc8KNF2OmSmn5iL5sX00wVcVbbjE')


router.post("/create-checkout-session", async (req, res) => {
    const { priceId } = req.body;
    // See https://stripe.com/docs/api/checkout/sessions/create
    // for additional parameters to pass.
    try {
      const session = await stripe.checkout.sessions.create({
        mode: 'subscription',
        payment_method_types: ['card'],
        line_items: [
          {
            price: priceId,
            // For metered billing, do not pass quantity
            quantity: 1,
          },
        ],
        // {CHECKOUT_SESSION_ID} is a string literal; do not change it!
        // the actual Session ID is returned in the query parameter when your customer
        // is redirected to the success page.
        success_url: 'http://www.localhost:3000/api/stripes/order/success',
        cancel_url: 'https://example.com/cancel',
      });
      console.log(session.id);
      res.send({
        sessionId: session.id,
      });
    } catch (e) {
      res.status(400);
      return res.send({
        error: {
          message: e.message,
        }
      });
    }
  });

  router.post('/order/success', async (req, res) => {
    const session = await stripe.checkout.sessions.retrieve(req.query.session_id);
    const customer = await stripe.customers.retrieve(session.customer);
  
    res.send(`
      <html>
        <body>
          <h1>Thanks for your order, ${customer.name}!</h1>
        </body>
      </html>
    `);
  });
module.exports = router;