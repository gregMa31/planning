import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginModule } from './components/login/login.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ToastrModule } from 'ngx-toastr';
import { ErrorInterceptor } from 'src/_helpers/error.interceptor';
import { AuthInterceptorService } from 'src/_helpers/auth-interceptor.service';
import { PlanningModule } from './components/planning/planning.module';
import { MenuModule } from './components/menu/menu.module';
import { AdministrationModule } from './components/administration/administration.module';
import { MatSelectModule } from '@angular/material/select';
import { reservationListModule } from './components/reservation-list-user/reservation-list.module';
import { NgxStripeModule } from 'ngx-stripe';
import { CheckoutModule } from './stripe/checkout/checkout.module';
import { CreditCardComponent } from './stripe/credit-card/credit-card.component';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
@NgModule({
  declarations: [AppComponent, CreditCardComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    LoginModule,
    PlanningModule,
    reservationListModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    MenuModule,
    AdministrationModule,
    MatSelectModule,
    NgxStripeModule.forRoot(
      'pk_test_51M6pgxJvi3VtEHgUAiYXuOnfSWMnnc6iEsLDQy8WwfP8sckNL7O06bZcdnT4zwI37HWE3edHBTqZyS4PF84oZuRM00IXgODB0O'
    ),
    CheckoutModule,
  ],
  providers: [
    { provide: LocationStrategy, useClass: HashLocationStrategy },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptorService,
      multi: true,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
