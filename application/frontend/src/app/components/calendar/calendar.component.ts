import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  ElementRef,
  ViewChildren,
  QueryList,
  Renderer2,
} from '@angular/core';
import { ReservationService } from 'src/app/services/reservation.service';
import { AuthService } from 'src/app/services/auth.service';
import { Reservation } from 'src/app/models/reservation.model';
import { Prestation } from 'src/app/models/prestation.model';
import { SlotBooking } from 'src/app/models/slots';
import { AdministrationService } from 'src/app/services/administration.service';
import { Employe } from 'src/app/models/employe.model';
import { combineLatestWith, firstValueFrom, fromEvent, map } from 'rxjs';
import { PrestationService } from 'src/app/services/prestation.service';
import { CalendarService } from 'src/app/services/calendar.service';

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.scss'],
})
export class CalendarComponent implements OnInit {
  @ViewChildren('planning') planning: QueryList<ElementRef> | undefined;
  @Input() prestationSelected: Prestation = <Prestation>{};
  @Output() chooseEmploye = new EventEmitter<string>();
  public actualDay = new Date()
  public incrementNumberWeek: number = 0;
  public schedule: Reservation[] = [];
  public currentMonday: string = '';
  public currentMondayFrench: string = '';
  public hoursRange = [9, 10, 11, 12, 13, 14, 15, 16, 17, 18];
  public employees: Employe[] = [];
  public temporateBookedSlots: any;
  public reservation: SlotBooking[] = [];
  public topPositionInPx: number = 0;
  public selectedDate: Date = new Date();
  private prestations: Prestation[] = []
  constructor(
    private reservationService: ReservationService,
    private authService: AuthService,
    private renderer: Renderer2,
    private administrationService: AdministrationService,
    private prestationService: PrestationService,
    private elm: ElementRef,
    private calendarService: CalendarService
  ) { }

  async ngOnInit() {

    const data$ = this.administrationService.getEmployes()
      .pipe(
        combineLatestWith(
          this.prestationService.getAllPrestations()
        ),
        map(([employees, prestations]) => ({ employees, prestations }))
      )
    const { employees, prestations } = await firstValueFrom(data$)
    this.prestations = prestations
    this.employees = employees;
    this.reservationService.getReservations(this.selectedDate.toLocaleDateString('fr'));
    this.reservationService.reservations$.subscribe(reservations => {
      this.fetchReservationAndGenerateSlot(reservations)
    })
  }

  selectEmploye(employeId: string) {
    this.chooseEmploye.emit(employeId)
  }

  async fetchReservationAndGenerateSlot(reservations: Reservation[]) {
    this.reservation = []
    this.calendarService.removeEmployeCards(this.elm, this.renderer)

    this.employees.forEach((employe, index) => {

      let slotsBooked: number[] = []

      reservations.map(reservation => {
        if (reservation.id_employe === employe._id) {
          const reservationName = this.prestations.filter(prestation => prestation._id === reservation.id_prestation)[0].nom

          let topPixelPosition = (reservation.slot - this.hoursRange[0]) * 80 + 40
          let numberOfSlot = this.prestations.find(prestation => prestation._id === reservation.id_prestation)?.duree
          numberOfSlot = numberOfSlot ? numberOfSlot * 4 : 0

          slotsBooked = [...slotsBooked, ...this.calendarService.generateSlotListBooked(numberOfSlot, reservation.slot)]
          let extraStyle = `background-color: #1d1d1d;
          background-image: radial-gradient(circle farthest-corner at 100% 100%, rgba(122, 167, 255, 0.32), transparent 44%), radial-gradient(circle farthest-corner at 0% 100%, rgba(255, 1, 1, 0.15), transparent 32%);`
          const div = this.renderer.createElement('div');
          this.renderer.setAttribute(div, 'class', 'rendererCard');
          this.calendarService.generateCardSlot(this.renderer, div, topPixelPosition, numberOfSlot, index, extraStyle, true, this.planning?.toArray()[index], reservationName)
        }
      })
      this.reservation.push({
        employe_id: employe._id,
        slotsBooked: slotsBooked,
        slotsUnbooked: [],
        actualSlotFocus: 0,
        numberOfSlot: 0,
        slotToBook: [],
        firstIndexSlotsBooked: 0
      });
    });
    setTimeout(() => {
      this.addListenerFunctionCalendar();
    }, 0);

  }

  addListenerFunctionCalendar() {
    // var timesPerSecond = 5;
    // var wait = false;

    this.planning?.toArray().forEach((item, index) => {
      fromEvent(item.nativeElement,'mouseleave').subscribe(() => {
        this.calendarService.removeCreationSlot(item, this.renderer, this.temporateBookedSlots);
      })

      this.renderer.listen(item.nativeElement, 'mouseover', (event) => {

        // lance event à chaque passage sur un autre slot
        // if (!wait) {
        let cannotBeBooked = true;

        this.calendarService.removeCreationSlot(item, this.renderer, this.temporateBookedSlots);
        this.reservation[index].actualSlotFocus = Number(event.target.id);
        this.reservation[index].numberOfSlot = this.prestationSelected.duree * 4;
        this.reservation[index].slotToBook = [];
        this.reservation[index].slotsUnbooked = [];
        for (let i = 0; i < this.reservation[index].numberOfSlot; i++) {
          this.reservation[index].slotToBook.push(
            this.reservation[index].actualSlotFocus + 0.25 * i
          );
        }

        for (let i = 1; i < this.reservation[index].numberOfSlot; i++) {
          this.reservation[index].slotsUnbooked.push(
            this.hoursRange[this.hoursRange.length - 1] + 1 - 0.25 * i
          );
        }

        cannotBeBooked = this.reservation[index].slotToBook.some(
          (r) => this.reservation[index].slotsBooked.indexOf(r) >= 0
        ); // vérifie si le créneau à réserver n'empiètent pas sur un autre et qu'il y a de la place pour la réservation

        if (
          this.reservation[index].slotToBook[
          this.reservation[index].slotToBook.length - 1
          ] >=
          this.hoursRange[this.hoursRange.length - 1] + 1
        ) {
          // vérifie s'il n'est pas trop tard pour réserver ce créneau
          cannotBeBooked = true;
        }


        if (cannotBeBooked) {
          this.renderer.setStyle(item.nativeElement, 'cursor', 'not-allowed');
        } else {
          this.renderer.setStyle(item.nativeElement, 'cursor', 'pointer');

          this.topPositionInPx =
            (this.reservation[index].actualSlotFocus - this.hoursRange[0]) * 80 + 40;

          let extraStyle = `background-color:whitesmoke; box-shadow: 0 15px 50px rgba(0,0,0,0.2); margin-top:1px; color:black`
          this.temporateBookedSlots = this.renderer.createElement('div')
          this.calendarService.generateCardSlot(this.renderer, this.temporateBookedSlots, this.topPositionInPx, this.reservation[index].numberOfSlot, index, extraStyle, true, item, this.prestationSelected.nom)

        }

        //   wait = true;
        //   setTimeout(function () {
        //     wait = false;
        //   }, 1000 / timesPerSecond);
        // }
      });
    });
  }

  public bookSlot(index: number, employeId: string, event: any) {

    if (Object.keys(this.prestationSelected).length !== 0) {
      this.reservation[index].firstIndexSlotsBooked = Number(event.target.id)
      this.reservation[index].slotsBooked = [
        ...this.reservation[index].slotsBooked,
        ...this.reservation[index].slotToBook,

      ];
      this.calendarService.saveToDB(this.reservation[index].firstIndexSlotsBooked, employeId, this.authService.user$.getValue()._id, this.selectedDate, this.prestationSelected._id)
      let extraStyle = `background-color: #1d1d1d;
      background-image: radial-gradient(circle farthest-corner at 100% 100%, rgba(122, 167, 255, 0.32), transparent 44%), radial-gradient(circle farthest-corner at 0% 100%, rgba(255, 1, 1, 0.15), transparent 32%);`
      const div = this.renderer.createElement('div');
      this.renderer.setAttribute(div, 'class', 'rendererCard');
      this.calendarService.generateCardSlot(this.renderer, div, this.topPositionInPx, this.reservation[index].numberOfSlot, index, extraStyle, true, this.planning?.toArray()[index], this.prestationSelected.nom)
    }
  }
}
