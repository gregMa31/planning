import { Component, OnInit } from '@angular/core';
import { PrestationService } from 'src/app/services/prestation.service';
import { Employe } from 'src/app/models/employe.model';
import { AdministrationService } from 'src/app/services/administration.service';
import { Prestation } from 'src/app/models/prestation.model';
import { firstValueFrom } from 'rxjs';

@Component({
  selector: 'app-planning',
  templateUrl: './planning.component.html',
  styleUrls: ['./planning.component.scss'],
})
export class PlanningComponent implements OnInit {
  public prestations = <Prestation[]>[];
  public prestationSelected = <Prestation>{};
  public employes: Employe[] = [];
  public idEmploye: any;
  constructor(
    private prestationService: PrestationService,
    private administrationService: AdministrationService
  ) {}

  async ngOnInit() {
    this.employes = await firstValueFrom(
      this.administrationService.getEmployes()
    );
    const prestations = await firstValueFrom(
      this.prestationService.getAllPrestations()
    );
    this.prestations = prestations.map((prestation) => {
      prestation.image = `/assets/${prestation.nom}.png`;
      return prestation;
    });
  }

  selectEmploye(employeId: string) {
    this.idEmploye = employeId;
  }

  selectPrestation(prestation: Prestation) {
    this.prestationSelected = prestation;
  }

  deleteProperties(object: any, properties: Array<string>) {
    //not working
    for (let key in object) {
      if (properties.includes(key)) {
        delete object.key;
      }
    }
    return object;
  }
}
