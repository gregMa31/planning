import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewEncapsulation,
} from '@angular/core';
import { Prestation } from 'src/app/models/prestation.model';
import SwiperCore, { EffectCoverflow, Pagination } from 'swiper';

// install Swiper modules
SwiperCore.use([EffectCoverflow, Pagination]);
@Component({
  selector: 'app-swiper-custom',
  templateUrl: './swiper-custom.component.html',
  styleUrls: ['./swiper-custom.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class SwiperComponent implements OnInit {
  @Output() choosePrestion = new EventEmitter<Prestation>();
  @Input() prestations: Prestation[] = [];
  constructor() {}

  ngOnInit(): void {
    this.prestations.map(
      (prestation) =>
        (prestation.image = `../../../assets/${prestation.nom}.png`)
    );
  }

  emitPrestation(prestation: Prestation) {
    this.choosePrestion.emit(prestation);
  }
}
