import { NgModule } from '@angular/core';
import { SwiperComponent } from './swiper-custom.component';
import { SwiperModule } from 'swiper/angular';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
@NgModule({
  declarations: [SwiperComponent],
  imports: [
    SwiperModule,
    BrowserModule,
    CommonModule,
    FormsModule,
    MatButtonModule,
    MatCardModule,
  ],
  providers: [],
  exports: [SwiperComponent],
})
export class swiperCustomModule {}
