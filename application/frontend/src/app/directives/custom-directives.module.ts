import { NgModule } from '@angular/core';
import { InputDebounceDirective } from './input-debounce.directive';

@NgModule({
  imports: [],
  declarations: [InputDebounceDirective],
  exports: [InputDebounceDirective],
})
export class CustomDirectivesModule {}
