import {
  Input,
  Output,
  Directive,
  ElementRef,
  EventEmitter,
} from '@angular/core';
import { debounce, fromEvent, timer } from 'rxjs';

@Directive({
  selector: '[appInputDebounce]',
})
export class InputDebounceDirective {
  @Input('debounceDuration') debounce: number | undefined;
  @Output() sendInputText = new EventEmitter<string>();
  constructor(private el: ElementRef) {
    fromEvent(this.el.nativeElement, 'input')
      .pipe(debounce(() => timer(this.debounce ? this.debounce : 300)))
      .subscribe((event: InputEvent | unknown) => {
        // changer le any
        if (event instanceof InputEvent) {
          const filterValue = (event.target as HTMLInputElement).value;
          this.sendInputText.emit(filterValue);
        }
      });
  }

  // utilisation : appInputDebounce (sendInputText)="test($event)"
  // utilisation option temps : appInputDebounce [debounceDuration]="3000" (sendInputText)="test($event)"
  // inclure directivemodule dans le module : import { CustomDirectivesModule } from 'src/app/directives/custom-directives.module';
}
