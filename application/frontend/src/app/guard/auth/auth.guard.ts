import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  RouterStateSnapshot,
  Router,
} from '@angular/router';
import { firstValueFrom } from 'rxjs';
import { AuthService } from 'src/app/services/auth.service';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root',
})
export class AuthGuard implements CanActivate {
  constructor(
    public authService: AuthService,
    public router: Router,
    private toastr: ToastrService
  ) {}

  async canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Promise<boolean> {
    const user = await firstValueFrom(this.authService.getUser());
    const token = localStorage.getItem('token');
    const tokenValid = token
      ? await firstValueFrom(this.authService.checkToken(token || ''))
      : {};
    if (
      Object.keys(user).length === 0 &&
      Object.keys(tokenValid).length === 0
    ) {
      this.toastr.error('Vous devez être connecter pour aller sur cette page');
      this.router.navigate(['/signIn']);
    }
    return true;
  }
}
