import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  RouterStateSnapshot,
  UrlTree,
  Router,
} from '@angular/router';
import { firstValueFrom } from 'rxjs';
import { AuthService } from 'src/app/services/auth.service';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root',
})
export class HomeGuard implements CanActivate {
  constructor(
    public authService: AuthService,
    public router: Router,
    private toastr: ToastrService
  ) {}

  async canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Promise<boolean> {
    const user = await firstValueFrom(this.authService.getUser());
    const token = localStorage.getItem('token');
    const tokenValid = token
      ? await firstValueFrom(this.authService.checkToken(token || ''))
      : false;
    if (Object.keys(user).length !== 0 || tokenValid) {
      this.router.navigate(['/planning']);
    }
    return true;
  }
}
