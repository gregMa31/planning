interface daySlot {
  _id: string;
  hourSlot: number;
  isBooked: boolean;
  idUser?: string;
  idPrestation?: string;
  litteralHour?: string;
}

export interface dateBooking {
  day: string;
  slot: number;
}
export interface Reservation {
  date: Date;
  _id?: string;
  day: string;
  slot: number;
  id_prestation: string;
  id_employe: string;
  id_user: string;
}
