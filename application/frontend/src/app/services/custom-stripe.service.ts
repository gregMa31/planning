import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { switchMap } from 'rxjs';
import { StripeService } from 'ngx-stripe';

@Injectable()
export class CustomStripeService {
  constructor(private http: HttpClient, private stripeService: StripeService) {}

  checkout(priceId: string) {
    const data = { priceId: priceId };
    return this.http
      .post(
        'http://www.localhost:3000/api/stripes/create-checkout-session',
        data
      )
      .pipe(
        switchMap((session: any) => {
          return this.stripeService.redirectToCheckout({
            sessionId: session.id,
          });
        })
      );
  }
}
