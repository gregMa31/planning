import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Prestation } from '../models/prestation.model';
import { Router } from '@angular/router';
import { environment } from '../../environments/environment';
@Injectable()
export class PrestationService {
  constructor(private http: HttpClient, private router: Router) {}

  getAllPrestations() {
    return this.http.get<Prestation[]>(
      `${environment.SOCKET_ENDPOINT}/api/prestations/prestations`
    );
  }

  getPrestation(id: string) {
    return this.http.get<Prestation>(
      `${environment.SOCKET_ENDPOINT}/api/prestations/prestation/${id}`
    );
  }
}
