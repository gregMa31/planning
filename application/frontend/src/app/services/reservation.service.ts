import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Reservation } from '../models/reservation.model';
import { BehaviorSubject } from 'rxjs';
import { httpResponseCustom } from '../models/generic.model';
import { environment } from '../../environments/environment';
@Injectable()
export class ReservationService {
  public reservations$ = new BehaviorSubject<Reservation[]>([]);

  constructor(private http: HttpClient) {}

  saveReservation(param: Reservation) {
    return this.http.post<Reservation>(
      `${environment.SOCKET_ENDPOINT}/api/reservations/reservations`,
      param
    );
  }

  getReservations(date: string) {
    this.http
      .get<Reservation[]>(
        `${environment.SOCKET_ENDPOINT}/api/reservations/reservations/${date}`
      )
      .subscribe((reservations) => {
        this.reservations$.next([...reservations]);
      });
  }
  getReservationsOfUser(userID: string) {
    return this.http.get<Reservation[]>(
      `${environment.SOCKET_ENDPOINT}/api/reservations/reservations/userId/${userID}`
    );
  }

  getReservationOfEmploye(employeId: string, rangeDate: Date[]) {
    const param: any = {
      employeId,
      rangeDate,
    };
    return this.http.post<Reservation[]>(
      `${environment.SOCKET_ENDPOINT}/api/reservations/reservations/employeId`,
      param
    );
  }

  deleteReservation(reservationId: string) {
    return this.http.get<httpResponseCustom>(
      `${environment.SOCKET_ENDPOINT}/api/reservations/reservations/removeReservation/${reservationId}`
    );
  }
}
