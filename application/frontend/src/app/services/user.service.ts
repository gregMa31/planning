import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User, ReservationUser } from '../models/user.model';
import { Router } from '@angular/router';
import { environment } from '../../environments/environment';
@Injectable()
export class UserService {
  constructor(private http: HttpClient, private router: Router) {}

  addReservationToUser(reservationUser: ReservationUser) {
    return this.http.post<User>(
      `${environment.SOCKET_ENDPOINT}/api/users/addReservationToUser`,
      reservationUser
    );
  }
}
