import { Component, OnInit } from '@angular/core';
import { CustomStripeService } from 'src/app/services/custom-stripe.service';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.scss'],
})
export class CheckoutComponent implements OnInit {
  constructor(private customStripeService: CustomStripeService) {}

  ngOnInit(): void {}

  checkout(priceId: string) {
    // Check the server.js tab to see an example implementation
    this.customStripeService.checkout(priceId).subscribe((result) => {
      console.log(result);

      // If `redirectToCheckout` fails due to a browser or network
      // error, you should display the localized error message to your
      // customer using `error.message`.
      if (result.error) {
        alert(result.error.message);
      }
    });
  }
}

// import { HttpClient } from '@angular/common/http';
// import { Component, OnInit } from '@angular/core';
// import { StripeService } from 'ngx-stripe';
// import { switchMap } from 'rxjs';
// import { CustomStripeService } from 'src/app/services/custom-stripe.service';

// @Component({
//   selector: 'app-checkout',
//   templateUrl: './checkout.component.html',
//   styleUrls: ['./checkout.component.scss']
// })
// export class CheckoutComponent implements OnInit {

//   constructor(    private http: HttpClient,
//     private stripeService: StripeService) { }

//   ngOnInit(): void {}

//   checkout(priceId : string) {
//     // Check the server.js tab to see an example implementation
//       // this.customStripeService.checkout(priceId)
//       const data = {priceId : priceId}
//       this.http.post('http://www.localhost:3000/api/stripes/create-checkout-session', data )
//       .pipe(
//         switchMap((session:any) => {
//           return this.stripeService.redirectToCheckout({ sessionId: session.id })
//         })
//       )
//       .subscribe(result => {
//         console.log(result);

//         // If `redirectToCheckout` fails due to a browser or network
//         // error, you should display the localized error message to your
//         // customer using `error.message`.
//         if (result.error) {
//           alert(result.error.message);
//         }
//       });
//   }
// }
